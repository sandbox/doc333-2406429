<?php if (!empty($table)) : ?>
  <table>
    <tr>
      <th><?php print t("Taxonomy"); ?></th>
      <th><?php print t("Menu link"); ?></th>
      <th><?php print t("Operation"); ?></th>
    </tr>
    <?php foreach ($table as $relation) : ?>
      <tr>
        <td><?php print $relation['name_vocab']; ?></td>
        <td><?php print $relation['title_link']; ?></td>
        <td><?php print l(t("Delete relation"), 'admin/structure/menu_taxonomy/delete_relation/' . $relation['vid'] . '/' . $relation['mlid'], array('attributes' => array('class' => array('button')))); ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
<?php endif; ?>

<?php print render($content); ?>